FROM golang:1.9-alpine as builder

WORKDIR /go/src/discord-modbot
COPY . /go/src/discord-modbot
RUN apk update && apk add git && go get && go build && go install

FROM alpine
WORKDIR /bot
VOLUME /bot/config.json
COPY --from=builder /go/bin/discord-modbot /bot/modbot

CMD ["./modbot"]
