package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

var newConfig bool
var configFilename string
var serverRoles map[string][]*discordgo.Role

func init() {
	flag.BoolVar(&newConfig, "n", false, "Create a new, empty config file")
	flag.StringVar(&configFilename, "c", "config.json", "The JSON config file for the bot")
}

func main() {

	flag.Parse()

	if newConfig {
		newConf(configFilename)
	}

	loadConfig(configFilename)

	if config.Token == "" {
		log.Fatal("No token supplied!")
	}

	dg, err := discordgo.New("Bot " + config.Token)

	if err != nil {
		log.Fatal("Failed to initialize discord bot session: ", err)
	}

	dg.AddHandler(onGuildJoin)
	dg.AddHandler(onReady)
	dg.AddHandler(onMessage)

	// Events for role updates -> refresh role list for server
	dg.AddHandler(onGuildRoleCreate)
	dg.AddHandler(onGuildRoleUpdate)
	dg.AddHandler(onGuildRoleDelete)

	err = dg.Open()

	if err != nil {
		log.Fatal(err)
	}

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}

func onGuildJoin(s *discordgo.Session, event *discordgo.GuildCreate) {
	log.Print("Joining guild: ", event.Guild.ID)
	if _, exists := config.Servers[event.Guild.ID]; !exists {
		config.Servers[event.Guild.ID] = ServerConf{Colors: []string{}, Games: []string{}}
		saveConfig(configFilename)
	}
	fetchGuildRoles(s, event.Guild.ID)
}

func onReady(s *discordgo.Session, event *discordgo.Ready) {
	// populate the serverRoles with all the roles in every guild
	serverRoles = make(map[string][]*discordgo.Role)

	for _, guild := range event.Guilds {
		serverRoles[guild.ID] = guild.Roles
	}

	s.UpdateStatus(0, prefix+"help")

	log.Printf("Ready!")
}

func onMessage(s *discordgo.Session, msg *discordgo.MessageCreate) {

	// ignore messages sent by bot itself
	if s.State.User.ID == msg.Author.ID {
		return
	}

	for cmdname, cmd := range commands {
		if strings.HasPrefix(msg.Content, prefix+cmdname) {
			// get channel from message
			c, err := s.State.Channel(msg.ChannelID)

			if err != nil {
				log.Print("Couldn't find channel")
				return
			}

			var params []string

			if len(msg.Content) > len(prefix+cmdname) {
				params = strings.Split(msg.Content[len(prefix+cmdname)+1:], " ")
			} else {
				params = make([]string, 0)
			}

			var emote string

			// call the command
			switch res := cmd(s, c.GuildID, msg.Message, params); res {
			case RESULT_SUCCESS:
				emote = "✅"
			case RESULT_ERROR:
				emote = "⁉"
			case RESULT_NOTALLOWED:
				emote = "🚫"
			}

			s.MessageReactionAdd(msg.ChannelID, msg.ID, emote)
			return
		}
	}

	// Wrong command
}

func onGuildRoleCreate(s *discordgo.Session, event *discordgo.GuildRoleCreate) {
	fetchGuildRoles(s, event.GuildID)
}

func onGuildRoleUpdate(s *discordgo.Session, event *discordgo.GuildRoleUpdate) {
	fetchGuildRoles(s, event.GuildID)
}

func onGuildRoleDelete(s *discordgo.Session, event *discordgo.GuildRoleDelete) {
	fetchGuildRoles(s, event.GuildID)
}

func fetchGuildRoles(s *discordgo.Session, guildID string) {
	roles, err := s.GuildRoles(guildID)

	if err != nil {
		log.Print(err)
		return
	}

	serverRoles[guildID] = roles
	saveConfig(configFilename)

	log.Printf("Updated guild roles for %s\n", guildID)
}
