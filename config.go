package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

type Config struct {
	Token   string                `json:"token"`
	Servers map[string]ServerConf `json:"servers"`
}

type ServerConf struct {
	Colors []string `json:"colors"`
	Games  []string `json:"games"`
}

var config Config

func loadConfig(file string) {

	data, err := ioutil.ReadFile(file)

	if err != nil {
		log.Fatal("Couldn't load config file: ", err)
	}

	err = json.Unmarshal(data, &config)

	if err != nil {
		log.Fatal(err)
	}
}

func saveConfig(file string) {
	json, err := json.MarshalIndent(config, "", "\t")

	if err != nil {
		log.Print(err)
	}

	if err := ioutil.WriteFile(file, []byte(json), 0644); err != nil {
		log.Print(err)
	}
}

func newConf(file string) {
	defer os.Exit(0)

	if _, err := ioutil.ReadFile(file); err == nil {
		fmt.Printf("There already exists a file called %s\n", file)
		return
	}

	config = Config{
		Token:   "",
		Servers: map[string]ServerConf{},
	}

	saveConfig(file)
}
