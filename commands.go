package main

import (
	"bytes"
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
)

const (
	RESULT_SUCCESS    = iota
	RESULT_ERROR      = iota
	RESULT_NOTALLOWED = iota
)

var prefix = "!"

var commands = map[string]func(*discordgo.Session, string, *discordgo.Message, []string) int{
	"color":       cmdChangeColor,
	"colour":      cmdChangeColor,
	"join":        cmdJoinRole,
	"leave":       cmdLeaveRole,
	"addcolor":    cmdAddColor,
	"delcolor":    cmdDelColor,
	"addgame":     cmdAddGame,
	"delgame":     cmdDelGame,
	"listcolors":  cmdListColors,
	"listcolours": cmdListColors,
	"listgames":   cmdListGames,
	"help":        cmdShowHelp,
}

func inArray(needle string, haystack []string) bool {
	for _, item := range haystack {
		// this is safe for numeric ID's, and can be used to test role
		// names case-insensitively
		if strings.EqualFold(item, needle) {
			return true
		}
	}

	return false
}

func deleteUserRoles(s *discordgo.Session, userID string, guildID string, roleNames []string) bool {
	// look up roles that user has
	member, err := s.State.Member(guildID, userID)

	if err != nil {
		log.Print("Failed to look up member", err)
		return false
	}

	for _, role := range serverRoles[guildID] {
		if inArray(role.ID, roleNames) && inArray(role.ID, member.Roles) {
			s.GuildMemberRoleRemove(guildID, userID, role.ID)
		}
	}

	return true
}

func isAuthenticated(s *discordgo.Session, user *discordgo.Member, guildID string) bool {

	for _, roleId := range user.Roles {
		for _, role := range serverRoles[guildID] {
			if role.ID == roleId && strings.EqualFold(role.Name, "admin") {
				return true
			}
		}
	}

	log.Printf("User %s#%s failed to authenticate\n", user.User.Username, user.User.Discriminator)

	return false
}

func cmdChangeColor(s *discordgo.Session, guildID string, msg *discordgo.Message, params []string) int {

	if len(params) != 1 {
		// not enough/too many arguments
		return RESULT_ERROR
	}

	param := strings.Join(params, " ")

	log.Print("Changing color of ", msg.Author.Username, " to ", param)

	// find role ID where role has name <param1>
	for _, role := range serverRoles[guildID] {
		if strings.EqualFold(param, role.Name) {
			deleteUserRoles(s, msg.Author.ID, guildID, config.Servers[guildID].Colors)
			s.GuildMemberRoleAdd(guildID, msg.Author.ID, role.ID)

			return RESULT_SUCCESS
		}
	}

	return RESULT_ERROR
}

func cmdJoinRole(s *discordgo.Session, guildID string, msg *discordgo.Message, params []string) int {

	if len(params) != 1 {
		return RESULT_ERROR
	}

	param := strings.Join(params, " ")

	for _, role := range serverRoles[guildID] {
		if strings.EqualFold(role.Name, param) && inArray(role.ID, config.Servers[guildID].Games) {
			s.GuildMemberRoleAdd(guildID, msg.Author.ID, role.ID)
			log.Printf("Adding '%s' to game '%s'\n", msg.Author.Username, param)
			return RESULT_SUCCESS
		}
	}

	return RESULT_ERROR
}

func cmdLeaveRole(s *discordgo.Session, guildID string, msg *discordgo.Message, params []string) int {
	if len(params) != 1 {
		return RESULT_ERROR
	}

	param := strings.Join(params, " ")

	member, err := s.GuildMember(guildID, msg.Author.ID)

	if err != nil {
		log.Print("Failed to look up guild member: ", err)
		return RESULT_ERROR
	}

	for _, role := range serverRoles[guildID] {
		if strings.EqualFold(role.Name, param) && inArray(role.ID, member.Roles) && inArray(role.ID, config.Servers[guildID].Games) {
			s.GuildMemberRoleRemove(guildID, msg.Author.ID, role.ID)
			log.Printf("Removing '%s' from game '%s'\n", msg.Author.Username, param)
			return RESULT_SUCCESS
		}
	}

	return RESULT_ERROR
}

func cmdAddColor(s *discordgo.Session, guildID string, msg *discordgo.Message, params []string) int {

	if len(params) != 1 {
		return RESULT_ERROR
	}

	member, err := s.GuildMember(guildID, msg.Author.ID)

	if err != nil {
		return RESULT_ERROR
	}

	if !isAuthenticated(s, member, guildID) {
		return RESULT_NOTALLOWED
	}

	param := strings.Join(params, " ")

	// search in server roles for correct role
	for _, role := range serverRoles[guildID] {
		if strings.EqualFold(role.Name, param) {
			servConf := config.Servers[guildID]
			servConf.Colors = append(servConf.Colors, role.ID)
			config.Servers[guildID] = servConf
			saveConfig(configFilename)
			log.Printf("%s#%s added color '%s' to config", msg.Author.Username, msg.Author.Discriminator, params[0])
			return RESULT_SUCCESS
		}
	}

	return RESULT_ERROR
}

func cmdDelColor(s *discordgo.Session, guildID string, msg *discordgo.Message, params []string) int {
	if len(params) != 1 {
		return RESULT_ERROR
	}

	member, err := s.GuildMember(guildID, msg.Author.ID)

	if err != nil {
		return RESULT_ERROR
	}

	if !isAuthenticated(s, member, guildID) {
		return RESULT_NOTALLOWED
	}

	param := strings.Join(params, " ")

	for _, role := range serverRoles[guildID] {
		if strings.EqualFold(role.Name, param) {
			// found server role with matching name

			servConf := config.Servers[guildID]

			for idx, confRole := range servConf.Colors {
				if confRole == role.ID {
					// slice idx out of array
					servConf.Colors = append(servConf.Colors[:idx], servConf.Colors[idx+1:]...)
					config.Servers[guildID] = servConf
					saveConfig(configFilename)
					log.Printf("%s#%s removed color '%s' from config", msg.Author.Username, msg.Author.Discriminator, param)
					return RESULT_SUCCESS
				}
			}
		}
	}

	return RESULT_ERROR
}

func cmdAddGame(s *discordgo.Session, guildID string, msg *discordgo.Message, params []string) int {
	if len(params) != 1 {
		return RESULT_ERROR
	}

	member, err := s.GuildMember(guildID, msg.Author.ID)

	if err != nil {
		return RESULT_ERROR
	}

	if !isAuthenticated(s, member, guildID) {
		return RESULT_NOTALLOWED
	}

	param := strings.Join(params, " ")

	for _, role := range serverRoles[guildID] {
		if strings.EqualFold(role.Name, param) && !inArray(role.ID, config.Servers[guildID].Games) {
			servConf := config.Servers[guildID]
			servConf.Games = append(servConf.Games, role.ID)

			config.Servers[guildID] = servConf
			log.Printf("%s#%s added game '%s' to config", msg.Author.Username, msg.Author.Discriminator, param)
			saveConfig(configFilename)

			return RESULT_SUCCESS
		}
	}

	return RESULT_ERROR
}

func cmdDelGame(s *discordgo.Session, guildID string, msg *discordgo.Message, params []string) int {
	if len(params) != 1 {
		return RESULT_ERROR
	}

	member, err := s.GuildMember(guildID, msg.Author.ID)

	if err != nil {
		return RESULT_ERROR
	}

	if !isAuthenticated(s, member, guildID) {
		return RESULT_NOTALLOWED
	}

	param := strings.Join(params, " ")

	for _, role := range serverRoles[guildID] {
		if strings.EqualFold(role.Name, param) {

			servConf := config.Servers[guildID]

			if !inArray(role.ID, servConf.Games) {
				return RESULT_ERROR
			}

			for idx, game := range servConf.Games {
				if game == role.ID {
					servConf.Games = append(servConf.Games[:idx], servConf.Games[idx+1:]...)

					config.Servers[guildID] = servConf
					saveConfig(configFilename)
					log.Printf("%s#%s removed game '%s' from config", msg.Author.Username, msg.Author.Discriminator, params[0])
					return RESULT_SUCCESS
				}
			}
		}
	}

	return RESULT_ERROR
}

func cmdListColors(s *discordgo.Session, guildID string, msg *discordgo.Message, params []string) int {

	var buffer bytes.Buffer
	currentFound := false

	member, err := s.GuildMember(guildID, msg.Author.ID)

	if err != nil {
		log.Print("Couldn't find member: ", err)
		return RESULT_ERROR
	}

	buffer.WriteString("**Available colors** :rainbow: :\n```\n")

	for _, color := range config.Servers[guildID].Colors {
		// look up color name
		for _, role := range serverRoles[guildID] {
			if role.ID == color {
				buffer.WriteString(role.Name)

				if !currentFound && inArray(role.ID, member.Roles) {
					buffer.WriteString("\t\t(current)")
					currentFound = true
				}

				buffer.WriteString("\n")
				break
			}
		}
	}

	buffer.WriteString("```")

	s.ChannelMessageSend(msg.ChannelID, buffer.String())

	return RESULT_SUCCESS
}

func cmdListGames(s *discordgo.Session, guildID string, msg *discordgo.Message, params []string) int {

	var buffer bytes.Buffer

	games := map[string]bool{}
	longestName := 0
	padding := 4

	member, err := s.GuildMember(guildID, msg.Author.ID)

	if err != nil {
		log.Print("Couldn't find member: ", err)
		return RESULT_ERROR
	}

	for _, game := range config.Servers[guildID].Games {
		// look up game name
		for _, role := range serverRoles[guildID] {
			if role.ID == game {
				games[role.Name] = inArray(role.ID, member.Roles)

				if len(role.Name) > longestName {
					longestName = len(role.Name)
				}

				break
			}
		}
	}
	buffer.WriteString("**Available games**: :video_game: \n```\n")

	for game, joined := range games {
		buffer.WriteString(game)
		if joined {
			buffer.WriteString(strings.Repeat(" ", longestName-len(game)+padding))
			buffer.WriteString("(joined)")
		}

		buffer.WriteString("\n")
	}

	buffer.WriteString("```")

	s.ChannelMessageSend(msg.ChannelID, buffer.String())

	return RESULT_SUCCESS
}

func cmdShowHelp(s *discordgo.Session, guildID string, msg *discordgo.Message, params []string) int {

	var buffer bytes.Buffer

	buffer.WriteString("**ModBot helpdesk**: :question: \n```\n")
	buffer.WriteString("!help            Show this help\n")
	buffer.WriteString("!listgames       Show all available games (and the ones you've joined)\n")
	buffer.WriteString("!listcolors      Show all available colors (and your current color)\n")
	buffer.WriteString("!color <color>   Set your current color to a different color\n")
	buffer.WriteString("!join <game>     Obtain a role specific to <game> (useful to be mentioned)\n")
	buffer.WriteString("!leave <game>    Remove a game role\n")

	member, err := s.GuildMember(guildID, msg.Author.ID)

	if err == nil {
		for _, servRole := range serverRoles[guildID] {
			if strings.EqualFold(servRole.Name, "admin") && inArray(servRole.ID, member.Roles) {
				buffer.WriteString("!addgame <game>  Make a game role addable\n")
				buffer.WriteString("!delgame <game>  Make a game non-addable (doesn't remove role)\n")
				buffer.WriteString("!addcolor <cr>   Make a colour-role (cr) addable\n")
				buffer.WriteString("!delcolor <cr>   Make a colour-role non-addable (doesn't remove role)\n")
			}
		}
	}

	buffer.WriteString("```\nUK-english also supported.\n")

	s.ChannelMessageSend(msg.ChannelID, buffer.String())

	return RESULT_SUCCESS
}
